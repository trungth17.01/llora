<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('FS_METHOD','direct');
define("FTP_HOST", 'localhost');
define("FTP_USER", 'admin');
define("FTP_PASS", '123456');

define( 'DB_NAME', 'llora');


/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'E9.eP>}a,Uu7k7w]-*$6O__Hi`rjB,l -`S0lJ0#,r# [z&fZ(y/Qg(v)QWrDUV&' );
define( 'SECURE_AUTH_KEY',  'evJ9z~iWlEL/k@=Tm!Ju,C9dG_dkT{O3=>!B{:Vew>Z.=>KyU!,zs?X6S]`X!VX=' );
define( 'LOGGED_IN_KEY',    '4n9.xD;>Wi/68RHG[)SS=VZ1ZmWxZ&b_QFRv`(>fB[/^T?!Auc?]5sw0-X|%R)ic' );
define( 'NONCE_KEY',        '8 UB1 <.p&TOBu@O{fL(axW,kQWatx%wSy)b!3y} ura93[G*y,OrZ*MHvNpC/AZ' );
define( 'AUTH_SALT',        'gCKw$a,{:gZRy uT28Kg>T1DN^[QDzD=Iy$KEq|8?[~$f/A*F56u9LC%sk`h%x57' );
define( 'SECURE_AUTH_SALT', '3vx,b#Z9Q9p1Y2q9p(;7g2x%MeXM*r@Tp@4l>_XR5E4H@5pzSD$T{dG7S7T)$FHo' );
define( 'LOGGED_IN_SALT',   '7}UMlIg<%C6YK35NTkD!;ccv*[I,Af1-q/U-lY@#5EW`W97Y%F9l?JuaqiGd*ZaC' );
define( 'NONCE_SALT',       'dS`^P4u?<_Fjh)<{8 uc9><<~+0ltPnl8fA>q;rs%X: LVdmJahjmNG1LbPeCe(o' );
    
define( 'UPLOADS', 'wp-content/uploads');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
    define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */

require_once( ABSPATH . 'wp-settings.php' );
