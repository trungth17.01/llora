<?php
// ------------------------------------------------------------------------
// Register widgetized areas
// ------------------------------------------------------------------------
    function keydesign_sidebars_register() {
		register_sidebar( array(
            'name' => esc_html__( 'Blog Sidebar', 'intact' ),
            'id' => 'blog-sidebar',
            'class' => '',
            'description' => esc_html__( 'Add widgets for the blog sidebar area. If none added, default sidebar widgets will be used.', 'intact' ),
            'before_widget' => '<div id="%1$s" class="blog_widget widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h5 class="widget-title"><span>',
            'after_title' => '</span></h5>',
        ) );
        register_sidebar( array(
            'name' => esc_html__( 'Shop Sidebar', 'intact' ),
            'id' => 'shop-sidebar',
            'class' => '',
            'description' => esc_html__( 'Add widgets for the shop sidebar area.', 'intact' ),
            'before_widget' => '<div id="%1$s" class="blog_widget widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h5 class="widget-title"><span>',
            'after_title' => '</span></h5>',
        ) );

        register_sidebar( array(
            'name' => esc_html__( 'Footer first widget area', 'intact' ),
            'id' => 'footer-first-widget-area',
            'class' => '',
            'description' => esc_html__( 'Add one widget for the first footer widget area.', 'intact' ),
            'before_widget' => '<div id="%1$s" class="footer_widget widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h5 class="widget-title"><span>',
            'after_title' => '</span></h5>',
        ) );

        register_sidebar( array(
            'name' => esc_html__( 'Footer second widget area', 'intact' ),
            'id' => 'footer-second-widget-area',
            'class' => '',
            'description' => esc_html__( 'Add one widget for the second footer widget area.', 'intact' ),
            'before_widget' => '<div id="%1$s" class="footer_widget widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h5 class="widget-title"><span>',
            'after_title' => '</span></h5>',
        ) );

        register_sidebar( array(
            'name' => esc_html__( 'Footer third widget area', 'intact' ),
            'id' => 'footer-third-widget-area',
            'class' => '',
            'description' => esc_html__( 'Add one widget for the third footer widget area.', 'intact' ),
            'before_widget' => '<div id="%1$s" class="footer_widget widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h5 class="widget-title"><span>',
            'after_title' => '</span></h5>',
        ) );

        register_sidebar( array(
            'name' => esc_html__( 'Footer fourth widget area', 'intact' ),
            'id' => 'footer-fourth-widget-area',
            'class' => '',
            'description' => esc_html__( 'Add one widget for the fourth footer widget area.', 'intact' ),
            'before_widget' => '<div id="%1$s" class="footer_widget widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h5 class="widget-title"><span>',
            'after_title' => '</span></h5>',
        ) );
    }

    add_action( 'widgets_init', 'keydesign_sidebars_register' );
?>
