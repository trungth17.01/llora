<?php $redux_ThemeTek = get_option( 'redux_ThemeTek' ); ?>
</div>
<footer id="footer"
    class="<?php if (isset($redux_ThemeTek['tek-footer-fixed'])) { if ($redux_ThemeTek['tek-footer-fixed'] == '1') { echo esc_html('fixed'); } else { echo esc_html('classic');} } ?>">
    <?php get_sidebar( 'footer' ); ?>
    <div class="lower-footer">
        <div class="container container-ft row">
            <div class="col-md-6 SignUp-left">
                <div class="SignUp">
                    Sign Up For Updates
                </div>
                <input class="form-control mr-sm-2 SignUp-email" type="search" placeholder="ENTER YOUR EMAIL ADDRESS"
                    aria-label="Search">
                <div class="pull-left">
                    <span><?php echo isset($redux_ThemeTek['tek-footer-text']) ? $redux_ThemeTek['tek-footer-text'] : '&copy; Intact All rights reserved'; ?></span>
                </div>
                <div class="pull-right">
                    <?php if ( has_nav_menu( 'keydesign-footer-menu' ) ) {
                     wp_nav_menu( array( 'theme_location' => 'keydesign-footer-menu', 'menu' => 'Footer Menu', 'depth' => 1, 'container' => false, 'menu_class' => 'nav navbar-footer', 'fallback_cb' => 'false' ) );
                  }
               ?>
                </div>
            </div>

            <div class="col-md-6 ft-right">
                <div class="row">
                   <div class="col-md-4">
                      <p> <a href="http://localhost/llora/?page_id=2607"> ABOUT US</a> </p>
                      <ul>
                         <li><a href="">Brand Philosophy</a></li>
                         <li><a href="">Founder</a></li>
                         <li><a href="">Stockists</a></li>
                         <li><a href="">Press</a></li>
                      </ul>
                   </div>
                   <div class="col-md-4">
                   <p>SOCIAL</p>
                      <ul>
                         <li><a href="">Instagram</a></li>
                         <li><a href="">Pinterest</a></li>
                         <li><a href="">Facebook</a></li>
                      </ul>
                   </div>
                   <div class="col-md-4">
                   <p>©llora 2019</p>
                      <!-- <ul>
                         <li><a href="">Instagram</a></li>
                         <li><a href="">Pinterest</a></li>
                         <li><a href="">Facebook</a></li>
                      </ul> -->
                   </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php if (isset($redux_ThemeTek['tek-backtotop']) && $redux_ThemeTek['tek-backtotop'] == "1") : ?>
<div class="back-to-top">
    <i class="fa fa-angle-up"></i>
</div>
<?php endif; ?>

<?php wp_footer(); ?>
</body>

</html>

<!-- <div class="pull-left">
    <span><?php echo isset($redux_ThemeTek['tek-footer-text']) ? $redux_ThemeTek['tek-footer-text'] : '&copy; Intact All rights reserved'; ?></span>
</div>
<div class="pull-right">
    <?php if ( has_nav_menu( 'keydesign-footer-menu' ) ) {
                     wp_nav_menu( array( 'theme_location' => 'keydesign-footer-menu', 'menu' => 'Footer Menu', 'depth' => 1, 'container' => false, 'menu_class' => 'nav navbar-footer', 'fallback_cb' => 'false' ) );
                  }
               ?>
</div> -->