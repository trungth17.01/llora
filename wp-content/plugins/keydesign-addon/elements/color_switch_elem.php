<?php
if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_tek_color_swtich extends WPBakeryShortCodesContainer {
    }
}
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_tek_color_swtich_single extends WPBakeryShortCode {
    }
}
if (!class_exists('tek_color_swtich')) {
    class tek_color_swtich extends KEYDESIGN_ADDON_CLASS
    {
        function __construct() {
            add_action('init', array($this, 'kd_color_swtich_init'));
            add_shortcode('tek_color_swtich', array($this, 'kd_color_swtich_container'));
            add_shortcode('tek_color_swtich_single', array($this, 'kd_color_swtich_single'));
        }
        // Element configuration in admin
        function kd_color_swtich_init() {
            // Container element configuration
            if (function_exists('vc_map')) {
                vc_map(array(
                    "name" => esc_html__("Color Switcher", "keydesign"),
                    "description" => esc_html__("Carousel color picker", "keydesign"),
                    "base" => "tek_color_swtich",
                    "class" => "",
                    "show_settings_on_create" => true,
                    "content_element" => true,
                    "as_parent" => array('only' => 'tek_color_swtich_single'),
                    "icon" => plugins_url('assets/element_icons/color-switcher.png', dirname(__FILE__)),
                    "category" => esc_html__("KeyDesign Elements", "keydesign"),
                    "js_view" => 'VcColumnView',
                    "params" => array(
                        array(
                            "type"          =>  "dropdown",
                            "class"         =>  "",
                            "heading"       =>  esc_html__("Enable autoplay","keydesign"),
                            "param_name"    =>  "cs_autoplay",
                            "value"         =>  array(
                                    "Off"   => "auto_off",
                                    "On"   => "auto_on"
                                ),
                            "save_always" => true,
                            "description"   =>  esc_html__("Carousel autoplay settings.", "keydesign")
                        ),

                        array(
                            "type"          =>  "dropdown",
                            "class"         =>  "",
                            "heading"       =>  esc_html__("Autoplay speed","keydesign"),
                            "param_name"    =>  "cs_autoplay_speed",
                            "value"         =>  array(
                                    "10s"   => "10000",
                                    "9s"   => "9000",
                                    "8s"   => "8000",
                                    "7s"   => "7000",
                                    "6s"   => "6000",
                                    "5s"   => "5000",
                                    "4s"   => "4000",
                                    "3s"   => "3000",
                                ),
                            "save_always" => true,
                            "dependency" =>	array(
                                "element" => "cs_autoplay",
                                "value" => array("auto_on")
                            ),
                            "description"   =>  esc_html__("Carousel autoplay speed.", "keydesign")
                        ),

                        array(
                            "type" => "textfield",
                            "class" => "",
                            "heading" => esc_html__("Extra class name", "keydesign"),
                            "param_name" => "color_switch_class",
                            "value" => "",
                            "description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "keydesign")
                        ),
                    )
                ));
                // Shortcode configuration
                vc_map(array(
                    "name" => __("Child Image", "keydesign"),
                    "base" => "tek_color_swtich_single",
                    "content_element" => true,
                    "as_child" => array('only' => 'tek_color_swtich'),
                    "icon" => plugins_url('assets/element_icons/child-image.png', dirname(__FILE__)),
                    "params" => array(
                        array(
                            "type" => "attach_image",
                            "heading" => esc_html__("Upload image", "keydesign"),
                            "param_name" => "color_switch_image",
                            "admin_label" => true,
                            "description" => esc_html__("Upload new image here.", "keydesign")
                        ),
                        array(
                            "type" => "colorpicker",
                            "class" => "",
                            "heading" => esc_html__("Element Color", "keydesign"),
                            "param_name" => "color_switch_color",
                            "value" => "",
                            "description" => esc_html__("Choose color.", "keydesign")
                        )
                    )
                ));
            }
        }

        public function kd_color_swtich_container($atts, $content = null) {
            extract(shortcode_atts(array(
                'cs_autoplay' => '',
                'cs_autoplay_speed' => '',
                'color_switch_class'		=> ''
            ), $atts));

            $kd_colorsw_id = "kd-colorsw-".uniqid();

      			$output = '<div class="slider color-swtich '.$kd_colorsw_id.' '.$color_switch_class.'">'.do_shortcode($content).'</div>';

            $output .= '<script type="text/javascript">
              jQuery(document).ready(function($){
                if ($(".color-swtich.'.$kd_colorsw_id.'").length > 0) {
                  $(".color-swtich.'.$kd_colorsw_id.'").owlCarousel({
                    navigation: false,
                    pagination: true,
                    responsive: false,
                    transitionStyle : "fade",
                    items: 1,';
                    if($cs_autoplay == "auto_on") {
                      $output .= 'autoPlay: '.$cs_autoplay_speed.',';
                    }

                    $output .= 'addClassActive: true,
                    animateIn: "fadeIn",
                  });
                  var elem_count = 0;
                  jQuery(".color-swtich.'.$kd_colorsw_id.' .color-swtich-content").each(function (index, value) {
                     var elem_color = ( jQuery(this).attr("data-color") );
                     jQuery(".color-swtich.'.$kd_colorsw_id.' .owl-controls .owl-page span").eq(elem_count++).css( "background", elem_color );
                  });
                }

              });
            </script>';

      			return $output;
        }

        public function kd_color_swtich_single($atts, $content = null) {
            extract(shortcode_atts(array(
                'color_switch_image'          => '',
                'color_switch_color'          => ''
            ), $atts));

            $image  = wpb_getImageBySize($params = array(
                'post_id' => NULL,
                'attach_id' => $color_switch_image,
                'thumb_size' => 'full',
                'class' => ""
            ));

            $output = '<div class="color-swtich-content" data-color="'.$color_switch_color.'">'.$image['thumbnail'].'</div>';

			return $output;
        }
    }
}
if (class_exists('tek_color_swtich')) {
    $tek_color_swtich = new tek_color_swtich;
}
?>
